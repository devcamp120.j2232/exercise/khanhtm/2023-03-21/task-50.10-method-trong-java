public class Task5010 {
    public static void main(String[] args) {
        int sum = sumNumbersV1();
        System.out.println("Tổng của 100 số tự nhiên đầu tiên là: " + sum);
        int[] number1 = {1, 5, 10};
    int[] number2 = {1, 2, 3, 5, 7, 9};
    
    int sum1 = sumNumbersV2(number1);
    int sum2 = sumNumbersV2(number2);
    
    System.out.println("Tổng của mảng number1 là: " + sum1);
    System.out.println("Tổng của mảng number2 là: " + sum2);
    printHello(24);
    printHello(99);
    }
    public static int sumNumbersV1() {
        int sum = 0;
        for (int i = 1; i <= 100; i++) {
            sum += i;
        }
        return sum;
    }

    public static int sumNumbersV2(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        return sum;
    }
    public static void printHello(int number) {
        if (number % 2 == 0) {
            System.out.println("Đây là số chẵn");
        } else {
            System.out.println("Đây là số lẻ");
        }
    }
}
