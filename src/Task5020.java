import java.util.Arrays;
import java.util.ArrayList;
import java.util.Random;
public class Task5020 {
    public static void main(String[] args) {

        //task1
        String str = "Devcamp";
        int[] arr = {1, 2, 3};
        String[] parts = "Devcamp User".split(" ");

        System.out.println(Task1(str));   // false
        System.out.println(Task1(arr));   // true
        System.out.println(Task1(parts)); // true

        //task2
        int[] arr1 = {1, 2, 3, 4, 5, 6};
        int[] arr2 = {1, 2, 3, 4, 5, 6};

        System.out.println(Task2(arr1, 3)); 
        System.out.println(Task2(arr2, 5));

        //task3
        int[] arr3 = {3, 8, 7, 6, 5, -4, -3, 2, 1};
        Arrays.sort(arr3);
        System.out.println(Arrays.toString(arr3));
        
        //task4
        int[] arr4 = {1, 2, 3, 4, 5, 6};
        int n1 = 3;
        int index1 = Task4(arr4, n1);
        if (index1 == -1) {
            System.out.println("Gia tri của n khong hop le");
        } else {
            System.out.println("vi tri cua phan tu thu " + n1 + " la " + index1);
        }
        int[] arr5 = {1, 2, 3, 4, 5, 6};
        int n2 = 7;
        int index2 = Task4(arr5, n2);
        if (index2 == -1) {
            System.out.println("Gia tri của n khong hop le");
        } else {
            System.out.println("vi tri cua phan tu thu" + n2 + " là " + index2);
        }

        //task5
        int[] arr6 = {1, 2, 3};
        int[] arr7 = {4, 5, 6};
        int[] result = Task5(arr6, arr7);
        System.out.println(Arrays.toString(result));
        //task6
        Object[] arr100 = {Double.NaN, 0, 15, false, -22, "", null, 47, null};
        Object[] result1 = Task6(arr100);
        for (Object obj : result1) {
            System.out.println(obj);
        }


        //task7
        int[] arr71 = {2, 5, 9, 6};
        int[] arr72 = {2, 9, 6};
        int[] arr73 = {2, 5, 9, 6};
        int n71 = 5;
        int n72 = 5;
        int n73 = 6;

        int[] result71 = Task7(arr71, n71);
        int[] result72 = Task7(arr72, n72);
        int[] result73 = Task7(arr73, n73);

        System.out.println(Arrays.toString(result71)); // [2, 9, 6]
        System.out.println(Arrays.toString(result72)); // [2, 9, 6]
        System.out.println(Arrays.toString(result73)); // [2, 5, 9, 6]


        //task8
        int[] arr8 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int randomIndex = Task8(arr8.length);
        int randomElement = arr8[randomIndex];
        System.out.println(randomElement);

        //task9
        int x = 6;
        int y = 0;
        int x2 = 4;
        int y2 = 11;
        int[] arr9 = new int[x];

        for (int i = 0; i < x; i++) {
            arr9[i] = y;
        }
        System.out.println(Arrays.toString(arr9));

        int[] arr91 = new int[x2];
        for (int i = 0; i < x2; i++) {
            arr91[i] = y2;
        }
        System.out.println(Arrays.toString(arr91));

        //task10
        int x10 = 1;
        int y10 = 4;
        int[] arr10 = new int[y10];

        for (int i = 0; i < y10; i++) {
            arr10[i] = x10 + i;
        }

        System.out.println(Arrays.toString(arr10));

            int x101 = -6;
            int y101 = 4;
            int[] arr101 = new int[y101];
    
            for (int i = 0; i < y101; i++) {
                arr101[i] = x101 + i;
            }
    
            System.out.println(Arrays.toString(arr101));
    
    }
    public static boolean Task1(Object obj) {
        return obj instanceof Object[] || obj instanceof int[] || obj instanceof long[] || obj instanceof double[]
                || obj instanceof float[] || obj instanceof boolean[] || obj instanceof byte[] || obj instanceof short[];
    }

    public static int Task2(int[] arr, int n) {
        if (n < 0 || n >= arr.length) {
            throw new IndexOutOfBoundsException();
        }
    
        return arr[n];
    }

    public static int Task4(int[] arr, int n) {
        if (n < 1 || n > arr.length) {
            return -1;
        }
        return n - 1;
    }

    public static int[] Task5(int[] arr1, int[] arr2) {
        int[] result = new int[arr1.length + arr2.length];
        System.arraycopy(arr1, 0, result, 0, arr1.length);
        System.arraycopy(arr2, 0, result, arr1.length, arr2.length);
        return result;
    }

    public static Object[] Task6(Object[] arr) {
        ArrayList<Object> result = new ArrayList<>();
        for (Object obj : arr) {
            if (obj != null) {
                if (obj instanceof Number) {
                    if (!Double.isNaN(((Number) obj).doubleValue())) {
                        result.add(obj);
                    }
                } else if (obj instanceof String) {
                    String str = (String) obj;
                    if (!str.isEmpty() && !"undefined".equals(str)) {
                        try {
                            Double.parseDouble(str);
                            result.add(obj);
                        } catch (NumberFormatException e) {
                            // do nothing
                        }
                    }
                }
            }
        }
        return result.toArray();
    }

    public static int[] Task7(int[] arr, int n) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == n) {
                count++;
            }
        }
        int[] result = new int[arr.length - count];
        int j = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != n) {
                result[j] = arr[i];
                j++;
            }
        }
        return result;
    }

    public static int Task8(int length) {
        Random random = new Random();
        return random.nextInt(length);
    }
}